class CommentsController < ApplicationController

  def create
    comment = Comment.new(params[:comment])
    if comment.save
      render :json => comment
    else
      render :json => comment.errors, :status => :unprocessable_entity
    end
  end

  def update
    comment = comment.find(params[:id])
    if comment.update_attributes(params[:comment])
      render :json => comment
    else
      render :json => comment.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    comment = comment.find(params[:id])
    comment.destroy

    head :ok
  end
end
