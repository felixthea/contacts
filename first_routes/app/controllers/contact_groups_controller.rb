class ContactGroupsController < ApplicationController

  def create
    contact_group = ContactGroup.new(params[:contact_group])
    if contact_group.save
      render :json => contact_group
    else
      render :json => contact_group.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    contact_group = ContactGroup.find(params[:id])
    contact_group.destroy

    head :ok
  end

end
