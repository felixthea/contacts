class Comment < ActiveRecord::Base
  attr_accessible :contact_id, :text, :author_id

  validates :contact_id, :text, :author_id, presence: true

  belongs_to(
    :contact,
    class_name: "Contact",
    foreign_key: :contact_id,
    primary_key: :id
  )

  belongs_to(
    :author,
    class_name: "User",
    foreign_key: :author_id,
    primary_key: :id
  )
end
