class ContactShare < ActiveRecord::Base
  attr_accessible :contact_id, :sharee_id, :sharer_id

  validates :contact_id, :sharee_id, presence: true

  belongs_to(
    :sharee,
    class_name: "User",
    foreign_key: :sharee_id,
    primary_key: :id
  )

  belongs_to(
    :sharer,
    class_name: "User",
    foreign_key: :sharer_id,
    primary_key: :id
  )

  belongs_to(
    :contact,
    class_name: "Contact",
    foreign_key: :contact_id,
    primary_key: :id
  )
end
