class Contact < ActiveRecord::Base
  attr_accessible :email, :name, :user_id, :favorite

  validates :email, :name, :user_id, :favorite, presence: true

  belongs_to(
    :user,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
    :contact_shares,
    class_name: "ContactShare",
    foreign_key: :contact_id,
    primary_key: :id
  )

  has_many(
    :contact_groups,
    class_name: "ContactGroup",
    foreign_key: :contact_id,
    primary_key: :id
  )

  has_many(
    :comments,
    class_name: "Comment",
    foreign_key: :contact_id,
    primary_key: :id
  )

  def self.contacts_for_user_id(user_id)
    query = <<-SQL
      SELECT
        contacts.*
      FROM
        contacts
      LEFT OUTER JOIN
        contact_shares
      ON
        contacts.id = contact_shares.contact_id
      WHERE
        contacts.user_id = ?
      OR
        contact_shares.sharee_id = ?
    SQL

    Contact.find_by_sql([query, user_id, user_id])
  end

  def self.favorite_contacts_for_user_id(user_id)
    query = <<-SQL
      SELECT
        contacts.*
      FROM
        contacts
      LEFT OUTER JOIN
        contact_shares
      ON
        contacts.id = contact_shares.contact_id
      WHERE
        (contacts.user_id = 3 OR contact_shares.sharee_id = 3)
      AND
        contacts.favorite = 't'
    SQL

    Contact.find_by_sql([query, user_id, user_id])
  end
end
