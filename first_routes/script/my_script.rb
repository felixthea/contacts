require 'addressable/uri'
require 'rest-client'

=begin
# index

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users'
).to_s

puts RestClient.get(url)

# show

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/2'
).to_s

puts RestClient.get(url)

# create

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users'
).to_s

puts RestClient.post(url, :user => { :name => "Clark Kent", :email => "clarkkent@dailyplanet.com" })



# update

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/3'
).to_s

puts RestClient.put(url, :user => { :name => "Superman", :email => "superman@krypton.com" })

# destroy

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/2'
).to_s

puts RestClient.delete(url)
=end

#INDEX
url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: 'users/3/contacts'
).to_s

puts RestClient.get(url)

=begin
#SHOW
url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: 'contacts/1'
).to_s

puts RestClient.get(url)

#CREATE
url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/contacts'
).to_s

puts RestClient.post(url, contact: { email: "kalel@krypton.com", name: "Superman" , user_id: 1})

#CREATE
url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/contact_shares'
).to_s

puts RestClient.post(url, contact_share: { contact_id: 1, sharer_id: 1, sharee_id: 2})

#DELETE
url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/contact_shares/3'
).to_s


puts RestClient.delete(url)

=end


