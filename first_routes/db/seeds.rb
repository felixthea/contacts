# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(name: "Felix Thea", email: "felixthea@gmail.com")
User.create(name: "David Park", email: "parkdy6@gmail.com")
User.create(name: "Superman", email: "kalel@krypton.com")

Contact.create(name: "David Park", email: "parkdy6@gmail.com", user_id: 1)
Contact.create(name: "John Smith", email: "johnsmith@gmail.com", user_id: 1)
Contact.create(name: "Batman", email: "brucewayne@wayneenterprises.com", user_id: 1)

ContactShare.create(contact_id: 1, sharer_id: 1, sharee_id: 3)

Comment.create(contact_id: 1, text: "This is a great contact!", author_id: 1)
Comment.create(contact_id: 1, text: "Superman says: this is a great contact!", author_id: 3)

ContactGroup.create(contact_id: 1, user_id: 1, group_id: 1)
ContactGroup.create(contact_id: 2, user_id: 1, group_id: 1)
ContactGroup.create(contact_id: 3, user_id: 1, group_id: 2)