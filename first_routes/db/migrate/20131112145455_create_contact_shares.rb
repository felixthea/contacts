class CreateContactShares < ActiveRecord::Migration
  def change
    create_table :contact_shares do |t|
      t.integer :contact_id, null: false
      t.integer :sharee_id, null: false
      t.integer :sharer_id, null: false

      t.timestamps
    end
  end
end
