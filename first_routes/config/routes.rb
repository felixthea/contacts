FirstRoutes::Application.routes.draw do
  resources :users do
    resources :contacts, only: :index
    member do
      get 'favorite_contacts'
    end
  end
  resources :contacts, except: :index do
    resources :comments
  end
  resources :contact_shares, only: [:create, :destroy]
  resources :contact_groups, only: [:create, :destroy]
end
